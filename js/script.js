// 1. Document Object Model (DOM) - це структуроване представлення веб-сторінки або документа у вигляді дерева об'єктів. DOM представляє всі елементи веб-сторінки, такі як елементи HTML, атрибути, текстові вузли та інші, як об'єкти з взаємозв'язками між ними. Це програмний інтерфейс, який надає можливість змінювати структуру, стиль та зміст веб-сторінки за допомогою скриптів.

// 2. Властивості innerHTML та innerText використовуються для отримання або зміни текстового вмісту елементів у HTML. Основна різниця полягає в обробці HTML-структури. innerHTML розпізнає та створює HTML-елементи, тоді як innerText працює лише з текстовим вмістом і не розпізнає HTML-розмітку. Вибір між ними залежить від того, який тип вмісту ви хочете отримати або встановити.

// 3. Є кілька способів звернутися до елементів сторінки за допомогою JavaScript. Ось декілька найпоширеніших способів:
// getElementById;
// getElementsByClassName;
// getElementsByTagName;
// querySelector;
// querySelectorAll;


// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((paragraph) => {
  paragraph.style.backgroundColor = '#ff0000';
});

// Знайти елемент із id="optionsList" та вивести у консоль
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

// Знайти батьківський елемент та вивести в консоль
const parentElement = optionsList.parentElement;
console.log(parentElement);

// Знайти дочірні ноди та вивести в консоль назви та тип нод
const childNodes = optionsList.childNodes;
childNodes.forEach((node) => {
  console.log('Назва ноди:', node.nodeName, 'Тип ноди:', node.nodeType);
});

// Встановити в якості контента елемента з класом testParagraph наступний параграф
const testParagraph = document.querySelector('.testParagraph');
testParagraph.textContent = 'This is a paragraph';

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
const headerElements = mainHeader.querySelectorAll('*');
headerElements.forEach((element) => {
  console.log(element);
  element.classList.add('nav-item');
});

// Знайти всі елементи із класом section-title та видалити цей клас у цих елементів
const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach((title) => {
  title.classList.remove('section-title');
});

